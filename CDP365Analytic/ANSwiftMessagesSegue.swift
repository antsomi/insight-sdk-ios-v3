import UIKit

open class ANSwiftMessagesSegue: UIStoryboardSegue {
    
    public enum ANLayout {
        
        /// The standard message view layout on top.
        case topMessage
        
        /// The standard message view layout on bottom.
        case bottomMessage
        
        /// A floating card-style view with rounded corners on top
        case topCard
        
        /// A floating tab-style view with rounded corners on bottom
        case topTab
        
        /// A floating card-style view with rounded corners on bottom
        case bottomCard
        
        /// A floating tab-style view with rounded corners on top
        case bottomTab
        
        /// A floating card-style view typically used with `.center` presentation style.
        case centered
    }
    
    /**
     Specifies how the view controller's view is installed into the
     containing message view.
     */
    public enum ANContainment {
        
        /**
         The view controller's view is installed for edge-to-edge display, extending into the safe areas
         to the device edges. This is done by calling `messageView.installContentView(:insets:)`
         See that method's documentation for additional details.
         */
        case content
        
        /**
         The view controller's view is installed for card-style layouts, inset from the margins
         and avoiding safe areas. This is done by calling `messageView.installBackgroundView(:insets:)`.
         See that method's documentation for details.
         */
        case background
        
        /**
         The view controller's view is installed for tab-style layouts, inset from the side margins, but extending
         to the device edge on the top or bottom. This is done by calling `messageView.installBackgroundVerticalView(:insets:)`.
         See that method's documentation for details.
         */
        case backgroundVertical
    }
    
    /// The presentation style to use. See the ANSwiftMessages.ANPresentationStyle for details.
    public var presentationStyle: ANSwiftMessages.ANPresentationStyle {
        get { return messenger.defaultConfig.presentationStyle }
        set { messenger.defaultConfig.presentationStyle = newValue }
    }
    
    /// The dim mode to use. See the ANSwiftMessages.ANDimMode for details.
    public var dimMode: ANSwiftMessages.ANDimMode {
        get { return messenger.defaultConfig.dimMode}
        set { messenger.defaultConfig.dimMode = newValue }
    }
    
    // duration
    public var duration: ANSwiftMessages.ANDuration {
        get { return messenger.defaultConfig.duration}
        set { messenger.defaultConfig.duration = newValue }
    }
    
    /// Specifies whether or not the interactive pan-to-hide gesture is enabled
    /// on the message view. The default value is `true`, but may not be appropriate
    /// for view controllers that use swipe or pan gestures.
    public var interactiveHide: Bool {
        get { return messenger.defaultConfig.interactiveHide }
        set { messenger.defaultConfig.interactiveHide = newValue }
    }
    
    /// Specifies an optional array of event listeners.
    public var eventListeners: [ANSwiftMessages.ANEventListener] {
        get { return messenger.defaultConfig.eventListeners }
        set { messenger.defaultConfig.eventListeners = newValue }
    }
    
    /**
     The view that is passed to `SwiftMessages.show(config:view:)` during presentation.
     The view controller's view is installed into `containerView`, which is itself installed
     into `messageView`. `SwiftMessagesSegue` does this installation automatically based on the
     value of the `containment` property. `BaseView` is the parent of `MessageView` and provides a
     number of configuration options that you may use. For example, you may configure a default drop
     shadow by calling `messageView.configureDropShadow()`.
     */
    public var messageView = ANBaseView()
    
    /**
     The view controller's view is embedded in `containerView` before being installed into
     `messageView`. This view provides configurable squircle (round) corners (see the parent
     class `CornerRoundingView`).
     */
    public var containerView: ANCornerRoundingView = ANCornerRoundingView()
    
    /**
     Specifies how the view controller's view is installed into the
     containing message view. See `Containment` for details.
     */
    public var containment: ANContainment = .content
    
    /**
     Supply an instance of `KeyboardTrackingView` to have the message view avoid the keyboard.
     */
    public var keyboardTrackingView: ANKeyboardTrackingView? {
        get {
            return messenger.defaultConfig.keyboardTrackingView
        }
        set {
            messenger.defaultConfig.keyboardTrackingView = newValue
        }
    }
    
    private var messenger = ANSwiftMessages()
    private var selfRetainer: ANSwiftMessagesSegue? = nil
    private lazy var hider = { return ANTransitioningDismisser(segue: self) }()
    
    private lazy var presenter = {
        return ANPresenter(config: messenger.defaultConfig, view: messageView, delegate: messenger)
    }()
    
    override open func perform() {
        selfRetainer = self
        destination.modalPresentationStyle = .custom
        destination.transitioningDelegate = self
        source.present(destination, animated: true, completion: nil)
    }
    
    override public init(identifier: String?, source: UIViewController, destination: UIViewController) {
        super.init(identifier: identifier, source: source, destination: destination)
        dimMode = .gray(interactive: true)
        messenger.defaultConfig.duration = .forever
    }
    
    fileprivate let safeAreaWorkaroundViewController = UIViewController()
}

extension ANSwiftMessagesSegue {
    public func configure(layout: ANLayout) {
        messageView.bounceAnimationOffset = 0
        containment = .content
        containerView.cornerRadius = 0
        containerView.roundsLeadingCorners = false
        messageView.configureDropShadow()
        switch layout {
        case .topMessage:
            messageView.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
            messageView.collapseLayoutMarginAdditions = false
            let animation = ANTopBottomAnimation(style: .top)
            animation.springDamping = 1
            presentationStyle = .custom(animator: animation)
        case .bottomMessage:
            messageView.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
            messageView.collapseLayoutMarginAdditions = false
            let animation = ANTopBottomAnimation(style: .bottom)
            animation.springDamping = 1
            presentationStyle = .custom(animator: animation)
        case .topCard:
            containment = .background
            messageView.layoutMarginAdditions = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            messageView.collapseLayoutMarginAdditions = true
            containerView.cornerRadius = 15
            presentationStyle = .top
        case .bottomCard:
            containment = .background
            messageView.layoutMarginAdditions = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            messageView.collapseLayoutMarginAdditions = true
            containerView.cornerRadius = 15
            presentationStyle = .bottom
        case .topTab:
            containment = .backgroundVertical
            messageView.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 10)
            messageView.collapseLayoutMarginAdditions = true
            containerView.cornerRadius = 15
            containerView.roundsLeadingCorners = true
            let animation = ANTopBottomAnimation(style: .top)
            animation.springDamping = 1
            presentationStyle = .custom(animator: animation)
        case .bottomTab:
            containment = .backgroundVertical
            messageView.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 10)
            messageView.collapseLayoutMarginAdditions = true
            containerView.cornerRadius = 15
            containerView.roundsLeadingCorners = true
            let animation = ANTopBottomAnimation(style: .bottom)
            animation.springDamping = 1
            presentationStyle = .custom(animator: animation)
        case .centered:
            containment = .background
            messageView.layoutMarginAdditions = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            messageView.collapseLayoutMarginAdditions = true
            containerView.cornerRadius = 15
            presentationStyle = .center
        }
    }
}

extension ANSwiftMessagesSegue: UIViewControllerTransitioningDelegate {
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let shower = ANTransitioningPresenter(segue: self)
        messenger.defaultConfig.eventListeners.append { [unowned self] in
            switch $0 {
            case .didShow:
                shower.completeTransition?(true)
            case .didHide:
                if let completeTransition = self.hider.completeTransition {
                    completeTransition(true)
                } else {
                    // Case where message is interinally hidden by SwiftMessages, such as with a
                    // dismiss gesture, rather than by view controller dismissal.
                    source.dismiss(animated: false, completion: nil)
                }
                self.selfRetainer = nil
            default: break
            }
        }
        return shower
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return hider
    }
}

extension ANSwiftMessagesSegue {
    private class ANTransitioningPresenter: NSObject, UIViewControllerAnimatedTransitioning {
        
        fileprivate private(set) var completeTransition: ((Bool) -> Void)?
        private weak var segue: ANSwiftMessagesSegue?
        
        fileprivate init(segue: ANSwiftMessagesSegue) {
            self.segue = segue
        }
        
        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
            return segue?.presenter.animator.showDuration ?? 0.5
        }
        
        func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
            guard let segue = segue,
                  let toView = transitionContext.view(forKey: .to) else {
                transitionContext.completeTransition(false)
                return
            }
            if #available(iOS 12, *) {}
            // This works around a bug in iOS 11 where the safe area of `messageView` (
            // and all ancestor views) is not set except on iPhone X. By assigning `messageView`
            // to a view controller, its safe area is set consistently. This bug has been resolved as
            // of Xcode 10 beta 2.
            segue.safeAreaWorkaroundViewController.view = segue.presenter.maskingView
            
            completeTransition = transitionContext.completeTransition
            let transitionContainer = transitionContext.containerView
            toView.translatesAutoresizingMaskIntoConstraints = false
            segue.containerView.addSubview(toView)
            segue.containerView.topAnchor.constraint(equalTo: toView.topAnchor).isActive = true
            segue.containerView.bottomAnchor.constraint(equalTo: toView.bottomAnchor).isActive = true
            segue.containerView.leadingAnchor.constraint(equalTo: toView.leadingAnchor).isActive = true
            segue.containerView.trailingAnchor.constraint(equalTo: toView.trailingAnchor).isActive = true
            // Install the `toView` into the message view.
            switch segue.containment {
            case .content:
                segue.messageView.installContentView(segue.containerView)
            case .background:
                segue.messageView.installBackgroundView(segue.containerView)
            case .backgroundVertical:
                segue.messageView.installBackgroundVerticalView(segue.containerView)
            }
            let toVC = transitionContext.viewController(forKey: .to)
            if let preferredHeight = toVC?.preferredContentSize.height,
               preferredHeight > 0 {
                segue.containerView.heightAnchor.constraint(equalToConstant: preferredHeight).with(priority: UILayoutPriority(rawValue: 950)).isActive = true
            }
            if let preferredWidth = toVC?.preferredContentSize.width,
               preferredWidth > 0 {
                segue.containerView.widthAnchor.constraint(equalToConstant: preferredWidth).with(priority: UILayoutPriority(rawValue: 950)).isActive = true
            }
            segue.presenter.config.presentationContext = .view(transitionContainer)
            segue.messenger.show(presenter: segue.presenter)
        }
    }
}

extension ANSwiftMessagesSegue {
    private class ANTransitioningDismisser: NSObject, UIViewControllerAnimatedTransitioning {
        
        fileprivate private(set) var completeTransition: ((Bool) -> Void)?
        private weak var segue: ANSwiftMessagesSegue?
        
        fileprivate init(segue: ANSwiftMessagesSegue) {
            self.segue = segue
        }
        
        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
            return segue?.presenter.animator.hideDuration ?? 0.5
        }
        
        func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
            guard let messenger = segue?.messenger else {
                transitionContext.completeTransition(false)
                return
            }
            completeTransition = transitionContext.completeTransition
            messenger.hide()
        }
    }
}
