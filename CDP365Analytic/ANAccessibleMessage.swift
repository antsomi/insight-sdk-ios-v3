import Foundation

public protocol ANAccessibleMessage {
    var accessibilityMessage: String? { get }
    var accessibilityElement: NSObject? { get }
    var additonalAccessibilityElements: [NSObject]? { get }
}
