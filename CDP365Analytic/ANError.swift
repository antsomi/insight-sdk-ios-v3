
import Foundation

enum ANSwiftMessagesError: Error {
    case cannotLoadViewFromNib(nibName: String)
    case noRootViewController
}
