import UIKit

public protocol ANBackgroundViewable {
    var backgroundView: UIView! { get }
}
