import UIKit

public protocol ANMarginAdjustable {
    var layoutMarginAdditions: UIEdgeInsets { get }
    var collapseLayoutMarginAdditions: Bool { get set }
    var bounceAnimationOffset: CGFloat { get set }    
}

