//
//  ANSender.swift
//  CDP365Analytic
//
//  Created by VietVK on 10/26/20.
//  Copyright © 2020 ANTSPROGRAMMATIC. All rights reserved.
//

import Foundation

class ANSender: ANDispatcher {
    
    private let session: URLSession
    private let timeout: TimeInterval
    private let configure: ANConfigure?
    private let serializer = ANEventAPISerializer()
    
    init(configure: ANConfiguration, timeout: TimeInterval = 5.0) {
        self.timeout = timeout
        self.session = URLSession.shared
        self.configure = configure.value
    }
    
    func send(event: ANEvent, success: @escaping () -> (), failure: @escaping (Error) -> ()) {
        if let config = configure,
            config.PortalID.count > 0,
            config.PropertyID.count > 0 {
            
            let jsonBody: Data
            do {
                jsonBody = try serializer.json(forEvent: hashIfNeed(config: config, event: event))
            } catch  {
                failure(error)
                return
            }
            let requestAnalytic = buildRequest(baseURL: config.AnalyticsURL, portalId: config.PortalID, propertyId: config.PropertyID, body: jsonBody)
            send(request: requestAnalytic, success: success, failure: failure)
            if config.IsLogDelivery {
                let requestDelivery = buildRequest(baseURL: config.DeliveryURL, portalId: config.PortalID, propertyId: config.PropertyID, body: jsonBody)
                send(request: requestDelivery, success: success, failure: failure)
            }
        }else {
            failure(SDKError(msg: "Invalid configure file."))
        }
    }
    
    func sendToFacebook(event: ANEvent, success: @escaping () -> (), failure: @escaping (Error) -> ()) {
        let jsonBody: Data
        do {
            jsonBody = try serializer.jsonFacebookEvent(forEvent: event)
        } catch  {
            failure(error)
            return
        }
        
        let request = buildRequestFacebook(baseURL: ANConfigurationKeys.URLFacebookEvents, token: "", body: jsonBody)
        send(request: request, success: success, failure: failure)
    }
    
    func sendToGoogle(event: ANEvent, success: @escaping () -> (), failure: @escaping (Error) -> ()) {
        let jsonBody: Data
        do {
            jsonBody = try serializer.jsonGoogleEvent(forEvent: event)
        } catch  {
            failure(error)
            return
        }
        
        let request = buildRequestGoogle(baseURL: ANConfigurationKeys.URLGoogleEvents, token: "", linkId: "", action: event.actionName, body: jsonBody)
        send(request: request, success: success, failure: failure)
    }
    
}

extension ANSender {
    
    private func hashIfNeed(config: ANConfigure, event: ANEvent) -> ANEvent{
        if event.categoryName.lowercased() == "user",
           let items = event.items, items.count > 0,
           let item = items.first {
            
            let user = NSMutableDictionary(dictionary: item)
            
            item.allKeys.forEach { key in
                if let key = key as? String {
                    config.Identify.CustomerFields.forEach {
                        if $0.field.lowercased() == key.lowercased() {
                            if let value = item[key] as? String {
                                if $0.hash_md5 {
                                    user.setObject(value.MD5, forKey: key as NSString)
                                }
                            }
                        }
                    }
                    if config.Identify.IdentifyID.field.lowercased() == key.lowercased() {
                        if let value = item[key] as? String {
                            user.setObject((config.Identify.IdentifyID.hash_md5) ? value.MD5 : value, forKey: "id" as NSString)
                        }
                    }else {
                        let value = "\(item["phone"] ?? "")\(item["email"] ?? "")"
                        user.setObject((config.Identify.IdentifyID.hash_md5) ? value.MD5 : value, forKey: "id" as NSString)
                    }
                }
                
            }
            return ANEvent(screenName: event.screenName, categoryName: event.categoryName, actionName: event.actionName, items: [user], extra: event.extra, dimension: event.dimension)
        }
        
        return event
    }
    
    private func buildRequest(baseURL: String, portalId: String, propertyId: String, contentType: String? = nil, body: Data? = nil) -> URLRequest {
        
        let queryStringParam = [
            ANConfigurationKeys.QueryParamsPortalID: portalId,
            ANConfigurationKeys.QueryParamsPropertyID : propertyId,
            ANConfigurationKeys.QueryParamsResponseType : "json",
            ANConfigurationKeys.QueryParamsFormat : "json"
        ]
        var urlComponent = URLComponents(string: baseURL)
        let queryItem = queryStringParam.map {URLQueryItem(name: $0.key, value: $0.value)}
        urlComponent?.queryItems = queryItem
        
        var request = URLRequest(url: (urlComponent?.url!)!, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: timeout)
        request.httpMethod = "POST"
        body.map { request.httpBody = $0 }
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        ANUtilities.defaultUserAgent.map { request.setValue($0, forHTTPHeaderField: "User-Agent") }
        
        return request
    }
    
    private func buildRequestFacebook(baseURL: String, token: String, body: Data? = nil) -> URLRequest {
        let queryParams = [
            "access_token": token
        ]
        var urlComponent = URLComponents(string: "\(baseURL)\(token)/activities")
        let queryItems = queryParams.map {URLQueryItem(name: $0.key, value: $0.value)}
        urlComponent?.queryItems = queryItems
        
        var request = URLRequest(url: (urlComponent?.url!)!, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: timeout)
        request.httpMethod = "POST"
        body.map { request.httpBody = $0 }
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        ANUtilities.defaultUserAgent.map { request.setValue($0, forHTTPHeaderField: "User-Agent") }
        
        return request
        
    }
    
    private func buildRequestGoogle(baseURL: String, token: String, linkId: String, action: String, body: Data? = nil) -> URLRequest {
        
        let queryParams = [
            "dev_token": token,
            "link_id": linkId,
            "app_event_type": action,
            "id_type": "idfa",
            "rdid": ANUtilities.deviceId,
            "lat": "0",
            "app_version": ANUtilities.applicationVersion,
            "os_version": ANUtilities.osVersion,
            "sdk_version": ANUtilities.sdkVersion,
            "currency_code": "VND",
            "timestamp": "\(Int(Date().timeIntervalSince1970))"
        ]
                
        var urlComponent = URLComponents(string: "\(baseURL)")
        let queryItems = queryParams.map {URLQueryItem(name: $0.key, value: $0.value)}
        urlComponent?.queryItems = queryItems
        
        var request = URLRequest(url: (urlComponent?.url!)!, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: timeout)
        request.httpMethod = "POST"
        body.map { request.httpBody = $0 }
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        ANUtilities.defaultUserAgent.map { request.setValue($0, forHTTPHeaderField: "User-Agent") }
        
        return request
        
    }
    
    
    private func send(request: URLRequest, success: @escaping ()->(), failure: @escaping (_ error: Error)->()) {
        let task = session.dataTask(with: request) { data, response, error in
            if let error = error {
                failure(error)
            } else {
                guard let data = data else { return }
                ANAdsResponse.processing(data: data)
                success()
            }
        }
        task.resume()
    }
}
