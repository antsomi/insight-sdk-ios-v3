import Foundation

public protocol ANIdentifiable {
    var id: String { get }
}
