import UIKit

open class ANWindowViewController: UIViewController
{
    fileprivate var window: UIWindow?
    
    let windowLevel: UIWindow.Level
    let config: ANSwiftMessages.ANConfig
    
    override open var shouldAutorotate: Bool {
        return config.shouldAutorotate
    }
    
    public init(windowLevel: UIWindow.Level?, config: ANSwiftMessages.ANConfig) {
        self.windowLevel = windowLevel ?? UIWindow.Level.normal
        self.config = config
        let view = ANPassthroughView()
        let window = ANPassthroughWindow(hitTestView: view)
        self.window = window
        super.init(nibName: nil, bundle: nil)
        self.view = view
        window.rootViewController = self
        window.windowLevel = windowLevel ?? UIWindow.Level.normal
        if #available(iOS 13, *) {
            window.overrideUserInterfaceStyle = config.overrideUserInterfaceStyle
        }
    }
    
    func install(becomeKey: Bool) {
        show(becomeKey: becomeKey)
    }

    @available(iOS 13, *)
    func install(becomeKey: Bool, scene: UIWindowScene?) {
        window?.windowScene = scene
        show(becomeKey: becomeKey, frame: scene?.coordinateSpace.bounds)
    }
    
    private func show(becomeKey: Bool, frame: CGRect? = nil) {
        guard let window = window else { return }
        window.frame = frame ?? UIScreen.main.bounds
        if becomeKey {
            window.makeKeyAndVisible()
        } else {
            window.isHidden = false
        }
    }
    
    func uninstall() {
        window?.isHidden = true
        window = nil
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return config.preferredStatusBarStyle ?? super.preferredStatusBarStyle
    }

    open override var prefersStatusBarHidden: Bool {
        return config.prefersStatusBarHidden ?? super.prefersStatusBarHidden
    }
}

extension ANWindowViewController {
    static func newInstance(windowLevel: UIWindow.Level?, config: ANSwiftMessages.ANConfig) -> ANWindowViewController {
        return config.windowViewController?(windowLevel, config) ?? ANWindowViewController(windowLevel: windowLevel, config: config)
    }
}
