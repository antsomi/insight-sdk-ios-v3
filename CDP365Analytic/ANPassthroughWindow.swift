import UIKit

class ANPassthroughWindow: UIWindow {

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view = hitTestView?.hitTest(point, with: event)
        return view == self ? nil : view
    }

    init(hitTestView: UIView) {
        self.hitTestView = hitTestView
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private weak var hitTestView: UIView?
}
