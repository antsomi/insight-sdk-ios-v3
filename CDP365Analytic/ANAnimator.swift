import UIKit

public typealias ANAnimationCompletion = (_ completed: Bool) -> Void

public protocol ANAnimationDelegate: class {
    func hide(animator: ANAnimator)
    func panStarted(animator: ANAnimator)
    func panEnded(animator: ANAnimator)
}

/**
 An option set representing the known types of safe area conflicts
 that could require margin adustments on the message view in order to
 get the layouts to look right.
 */
public struct ANSafeZoneConflicts: OptionSet {
    public let rawValue: Int

    public init(rawValue: Int) {
        self.rawValue = rawValue
    }

    /// Message view behind status bar
    public static let statusBar = ANSafeZoneConflicts(rawValue: 1 << 0)

    /// Message view behind the sensor notch on iPhone X
    public static let sensorNotch = ANSafeZoneConflicts(rawValue: 1 << 1)

    /// Message view behind home indicator on iPhone X
    public static let homeIndicator = ANSafeZoneConflicts(rawValue: 1 << 2)

    /// Message view is over the status bar on an iPhone 8 or lower. This is a special
    /// case because we logically expect the top safe area to be zero, but it is reported as 20
    /// (which seems like an iOS bug). We use the `overStatusBar` to indicate this special case.
    public static let overStatusBar = ANSafeZoneConflicts(rawValue: 1 << 3)
}

public class ANAnimationContext {

    public let messageView: UIView
    public let containerView: UIView
    public let safeZoneConflicts: ANSafeZoneConflicts
    public let interactiveHide: Bool

    init(messageView: UIView, containerView: UIView, safeZoneConflicts: ANSafeZoneConflicts, interactiveHide: Bool) {
        self.messageView = messageView
        self.containerView = containerView
        self.safeZoneConflicts = safeZoneConflicts
        self.interactiveHide = interactiveHide
    }
}

public protocol ANAnimator: class {

    /// Adopting classes should declare as `weak`.
    var delegate: ANAnimationDelegate? { get set }

    func show(context: ANAnimationContext, completion: @escaping ANAnimationCompletion)

    func hide(context: ANAnimationContext, completion: @escaping ANAnimationCompletion)

    /// The show animation duration. If the animation duration is unknown, such as if using `UIDynamnicAnimator`,
    /// then profide an estimate. This value is used by `SwiftMessagesSegue`.
    var showDuration: TimeInterval { get }

    /// The hide animation duration. If the animation duration is unknown, such as if using `UIDynamnicAnimator`,
    /// then profide an estimate. This value is used by `SwiftMessagesSegue`.
    var hideDuration: TimeInterval { get }
}

