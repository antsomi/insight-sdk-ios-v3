import UIKit

open class ANCornerRoundingView: UIView {

    /// Specifies the corner radius to use.
    @IBInspectable
    open var cornerRadius: CGFloat = 0 {
        didSet {
            updateMaskPath()
        }
    }

    @IBInspectable
    open var roundsLeadingCorners: Bool = false

    open var roundedCorners: UIRectCorner = [.allCorners] {
        didSet {
            updateMaskPath()
        }
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }

    private func sharedInit() {
        layer.mask = shapeLayer
    }

    private let shapeLayer = CAShapeLayer()

    override open func layoutSubviews() {
        super.layoutSubviews()
        updateMaskPath()
    }

    private func updateMaskPath() {
        let newPath = UIBezierPath(roundedRect: layer.bounds, byRoundingCorners: roundedCorners, cornerRadii: cornerRadii).cgPath

        if let foundAnimation = layer.findAnimation(forKeyPath: "bounds.size") {

            let animation = CABasicAnimation(keyPath: "path")
            animation.duration = foundAnimation.duration
            animation.timingFunction = foundAnimation.timingFunction
            animation.fromValue = shapeLayer.path
            animation.toValue = newPath
            shapeLayer.add(animation, forKey: "path")
            shapeLayer.path = newPath
        } else {
            // Update the `shapeLayer's` path  without animation
            shapeLayer.path = newPath
        }
    }

    private var cornerRadii: CGSize {
        return CGSize(width: cornerRadius, height: cornerRadius)
    }
}
