//
//  ANAdsResponse.swift
//  CDP365Analytic
//
//  Created by VietVK on 10/26/20.
//  Copyright © 2020 ANTSPROGRAMMATIC. All rights reserved.
//

import UIKit
import Foundation

class ANAdsResponse: NSObject {
    
    static func processing(data: Data) {
        do {
            let campaignResponse = try JSONDecoder().decode(ANResponseCampaign.self, from: data)
            if let campaigns = campaignResponse.campaigns, campaigns.count > 0 {
                ANAdsResponse.showAds(campaign: campaigns[0])
            }
            
        } catch {
            print("JSON Processing Failed")
        }
    }
    
    
    fileprivate static func showAds(campaign: ANCampaign) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(campaign.timeDelay ?? 0)) {
            switch UIApplication.shared.applicationState {
            case .background, .inactive:
                break
            case .active:
                
                let view = ANMessageView.viewFromNib(layout: .cardView)
                view.configureContent(campaign: campaign) { (_) in
                    ANSwiftMessages.hide()
                }
                view.onTappedDirectLink {
                    ANSwiftMessages.hide()
                }
                
                view.configureDropShadow()
                var config = ANSwiftMessages.defaultConfig
                switch campaign.positionId {
                case "full_screen":
                    config.presentationStyle = .fullscreen
                case "top":
                    config.presentationStyle = .top
                case "bottom":
                    config.presentationStyle = .bottom
                case "center":
                    config.presentationStyle = .center
                default:
                    config.presentationStyle = .top
                }
                
                config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
                config.duration = .forever
                config.interactiveHide = true
                ANSwiftMessages.show(config: config, view: view)
                
            default:
                break
            }
        }
    }
    
}
