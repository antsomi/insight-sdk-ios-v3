//
//  FacebookAnalyticService.swift
//  CDP365Analytic
//
//  Created by VietVK on 10/26/20.
//  Copyright © 2020 ANTSPROGRAMMATIC. All rights reserved.
//

import Foundation

class FacebookAnalyticService: CoreAnalyticService, FacebookProtocolService {
    
    private static var dispatcher: ANDispatcher = ANSender(configure: ANConfiguration())
    
    static func configure() {}
    
    static func logEvent(screenName: String) {}
    
    static func logEvent(categoryName: String, actionName: String, items: [NSDictionary]?, extra: NSDictionary?, dimension: [NSDictionary]?) {
        let event = ANEvent(categoryName: categoryName, actionName: actionName, items: items, extra: extra, dimension: dimension)
        dispatcher.sendToFacebook(event: event, success: {
            ANLogger.debug("Dispatched batch of event.")
        }, failure: { error in
            ANLogger.error(error)
        })
    }
    
    static func resetAnonymousId() {}
    
}
