import Foundation

extension Bundle {
    static func sm_frameworkBundle() -> Bundle {
        return Bundle(identifier: ANUtilities.sdkIdentifier)!
    }
}
